# Seen.io Fullstack Code Challenge
Coding challenge provided by seen.io for fullstack position. Fun and interesting challenge.

For structuring this project all code has been implemented into same folder for easier access. See ***Structure*** section below for more details. 

## Structure
For this project the backend and frontend is seperated into to folders. 

## Backend 

Contains a ASPNET core Web API using Singleton as service to hold and serve data through the REST API. Swagger is used for documentation of the API and can be found on https://localhost:44353/swagger/index.html unless changed in the ***launchsettings.json*** file under ***Properties*** folder.

Uses .NET 5.0 framework with no other NuGet packages required to run.

## Frontend

Created using <code>npx create-react-app {name} --template typescript</code>

> **Note**: Uses ***Yarn*** as package manager

Other packages:

- Styled-Components
- React-router-dom


## 5 hour mark
As the 5 hours mark and deadline comes faster than expected not all implementation has been made.

Pseudocode has been used to further carry the intent of what would be further implemented if time wasn't the case. 

Backend was first started on and as with all code, there could be changes that would save time for this solution.

Frontend was last implemented as it is required for rendering the data.