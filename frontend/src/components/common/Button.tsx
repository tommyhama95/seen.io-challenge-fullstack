import React from 'react';

// To be used as a general style for buttons with passing props of what methods the button
// should call upon when pressed, as well if it is set to disabled/enabled

const Button = () => {
    return (
        <button>
            
        </button>
    );
}

export default Button;
