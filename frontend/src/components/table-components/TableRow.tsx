import React from 'react';

const TableRow = ({user}: any) => {

    /*
        Loop over the objects key and value in order to render out each of the User's data
        and setting the key value for each td as the unique reference for react
        Ex: 1.id could be key for the td in id table data
    */

    const data: any[] = [];
    for(let [key, value] of Object.entries(user)) {
         data.push(value);
    }
    return (
        <tr>
            { 
                data.map(el => {
                    return <td key={"1."+el}>{el}</td>;
                }) 
            }
        </tr>
    );
}

export default TableRow;
