import React from 'react';
import { User } from '../../types';
import TableHeaders from './TableHeaders';
import TableRow from './TableRow';

const Table = ({users}: any) => {

    // TableHeaders uses first instance of a User object to make Table headers

    return (
        <table>
            <TableHeaders user={users[0]} />
            {
                users.map((user: User) => {
                    return <TableRow user={user}/>
                })
            }
        </table>
    );
}

export default Table;
