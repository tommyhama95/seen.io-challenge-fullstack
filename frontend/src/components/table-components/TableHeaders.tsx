import React from 'react';

const TableHeaders = ({user}: any) => {
    
    // Loop over each object key and use that for making table header names
    let headers: string[] = [];
    for(let [key] of Object.entries(user)) {
        headers.push(key);
    }

    return (
        <thead>
            <tr>
            {
                headers.map((key: string) => { 
                    return <th key={key}>{key}</th>
                })
            }
            </tr>
        </thead>
    );
}

export default TableHeaders;
