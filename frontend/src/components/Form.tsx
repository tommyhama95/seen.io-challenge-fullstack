import React from 'react';
import { MainContainer, PageTitle } from '../styles';

/*
    To be the component rendering out a form to submit a new user with possible 
    customized EventTracker starting values, would have to be also fixed in Backend API as it does not
    handle overriding start values.
*/

const Home = () => {
    return (
        <MainContainer>
            <PageTitle>Form</PageTitle>
        </MainContainer>
    );
}


export default Home;
