import React, { useEffect, useState } from 'react';
import { MainContainer, PageTitle } from '../styles';
import Table from './table-components/Table';
import { User } from './../types';

const EventTable = () => {
    let [users, setUsers] = useState<User[] | null>(null);
    
    useEffect( () => {
        fetch("https://localhost:44353/api/User")
            .then(response => response.json())
            .then(data => setUsers(data));
    }, []);

    return (
        <MainContainer>
            <PageTitle>Event table</PageTitle>
            <p>This table shows data on a users activity on Landing page visits, Video plays, etc.</p>
            {
                users && <Table users={users} />
            }
        </MainContainer>
    );
}

export default EventTable;
