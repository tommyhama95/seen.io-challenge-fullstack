import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

/*
    Header component using styled-components for rendering link switching
    between the two pages for submitting form or looking at table data
*/

const Header = () => {
    
    return (
        <StyledHeader>
            <h1>Event Tracker</h1>
            <StyledUl>
                <li>
                    <Link to="/">Form</Link>
                </li>
                <li>
                    <Link to="/events">Events</Link>
                </li>
            </StyledUl>
        </StyledHeader>
    );
}

const StyledHeader = styled.header`
    background-color: #2c2c2c;
    width: 100vw;
    min-height: 4rem;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: center;
    color: #e2e2e2;

    > h1 {
        margin: 0;
    }
`;

const StyledUl = styled.ul`
    width: 50%;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    gap: 5rem;

    > li {
        list-style: none;
        > a{
            text-decoration: none;
            font-size: 1.3rem;
            color: white;
        }
    }
    
`;



export default Header;

/*

display: grid;
grid-template-columns: 0.2fr 1fr 2fr 0.2fr;
grid-column: 2;
*/