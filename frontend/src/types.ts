//  User object
export type User = {
    id: number,
    email: string,
    phoneNumber: string,
    eventTrackerId: number
}

// EventTracker object
export type EventTracker = {
    id: number,
    landingPageVisits: number,
    videoPlays: number
}