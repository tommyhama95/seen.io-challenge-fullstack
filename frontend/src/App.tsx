import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import Header from "./components/Header";
import Form from "./components/Form";
import EventTable from "./components/EventTable";

// Component Form has two endpoints just for url convenient since "/" doesnt really
// explicitly tell a user that this endpoints is a form
function App() {
  return (
    <StyledApp>
      <Router>
        <Header />
        <Switch>
          <Route path="/events" component={EventTable}/>
          <Route path="/form" component={Form}/>
          <Route path="/" component={Form}/> 
        </Switch>
      </Router>
    </StyledApp>
  );
}

const StyledApp = styled.div`
  height: 100vh;
  width: 100vw;
`;

export default App;
