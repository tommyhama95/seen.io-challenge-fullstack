import styled from 'styled-components';

export const MainContainer = styled.main`
    margin: 1rem 3rem;
    height: calc(100% - 6rem);
    display: flex;
    flex-direction: column;
    align-items: center;
`;

export const PageTitle = styled.h2`
    font-size: 2.5rem;
    margin: 2rem 0 1rem 0;
`;