# Setting up Frontend - React

## Yarn
This solution uses yarn as the Package Manager as it comes default with <code>yarn create-react-app {name}</code>

To run:

Navigate from top folder to /Frontend and use 

>yarn install

then

> yarn start

and react should open on

> localhost:3000