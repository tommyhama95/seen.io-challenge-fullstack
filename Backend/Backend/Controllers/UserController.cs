﻿using Backend.Data.Storage;
using Backend.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IStorage _storage;

        public UserController(IStorage storage)
        {
            _storage = storage;
        }

        /// <summary>
        /// Gets a Users within the storage / in memory
        /// </summary>
        /// <returns>List of Users</returns>
        [HttpGet]
        public List<User> GetAllUsers()
        {
            return  _storage.GetAllUsers();
        }

        /// <summary>
        /// When provided an Id, get the User with the same Id if they exist in the application
        /// </summary>
        /// <param name="id">User Id of type int</param>
        /// <returns>Returns User if exists and 404 if the user doesnt exist</returns>
        [HttpGet("{id}")]
        public ActionResult<User> GetUserById(int id)
        {
            User user = _storage.GetUserById(id);

            if (user == null)
            {
                return NotFound();
            }
            return user;
        }

        /// <summary>
        /// Create a new User with the provided data with new EvenTracker data
        /// </summary>
        /// <param name="user">User object</param>
        /// <returns>Returns the newly created User or 409 if user exists</returns>
        [HttpPost]
        public ActionResult<User> PostUser(User user)
        {
            bool exists = _storage.UserExists(user);
            if(exists)
            {
                return Conflict();
            }
            return _storage.CreateUser(user);            
        }

    }
}
