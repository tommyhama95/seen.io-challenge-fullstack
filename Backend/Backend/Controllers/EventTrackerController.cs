﻿using Backend.Data.Storage;
using Backend.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventTrackerController : Controller
    {
        private readonly IStorage _storage;

        public EventTrackerController(IStorage storage)
        {
            _storage = storage;
        }

        /// <summary>
        /// Get all EvenTrackers registered on Users
        /// </summary>
        /// <returns>List of EventTrackers</returns>
        [HttpGet]
        public List<EventTracker> GetAllEventTrackers()
        {
            return _storage.GetAllEventTrackers();
        }

        /// <summary>
        /// When provided with Users' id, get the existing EventTracker for that User
        /// </summary>
        /// <param name="id">Users' Id</param>
        /// <returns>Returns Users' EvenTracker data</returns>
        [HttpGet("{id}")]
        public ActionResult<EventTracker> GetEventTrackerFromUserId(int id)
        {
            EventTracker eventT = _storage.GetEventTrackerById(id);

            if(eventT == null)
            {
                return NotFound();
            }
            return eventT;
        }

    }
}
