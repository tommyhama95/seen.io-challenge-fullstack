﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Model
{
    public class EventTracker
    {
        public int Id { get; set; }
        public int LandingPageVisits { get; set; } = 0;
        public int VideoPlays { get; set; } = 0;
    }
}
