﻿using Backend.Model;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Storage
{
    public interface IStorage
    {
        // User methods
        public List<User> GetAllUsers();
        public User GetUserById(int id);
        public User CreateUser(User user);
        public bool UserExists(User user);
        // EventTracker methods       
        public List<EventTracker> GetAllEventTrackers();
        public EventTracker GetEventTrackerById(int id);
    }
}
