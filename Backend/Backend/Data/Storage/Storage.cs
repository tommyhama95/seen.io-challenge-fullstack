﻿using Backend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Data.Storage
{
    public class Storage : IStorage
    {
        private List<User> Users = new()
        {
            new()
            {
                Id = 1,
                Email = "olaf.Olafsen@example.com",
                PhoneNumber = "11223344",
                EventTrackerId = 1
            },
            new()
            {
                Id = 2,
                Email = "BettyTheBestOffRouder@horse.com",
                PhoneNumber = "33332222",
                EventTrackerId = 2,
            },
            new()
            {
                Id = 3,
                Email = "Gunnar@gunnarson.no",
                PhoneNumber = "+4783246512",
                EventTrackerId = 3
            }
        };

        private List<EventTracker> EventTrackers = new()
        {
            new() { Id = 1 },
            new() { Id = 2 },
            new() { Id = 3 }
        };

        public Storage () { }


        public List<User> GetAllUsers()
        {
            return Users;
        }

        public User GetUserById(int id)
        {
            return Users.FirstOrDefault(user => user.Id == id);
        }

        public User CreateUser(User user)
        {
            Users.Add(user);
            EventTracker eventT = new() { Id = user.EventTrackerId };
            EventTrackers.Add(eventT);
            return user;
        }

        public bool UserExists(User user)
        {
            var userResult = Users.FirstOrDefault(u => u.Id == user.Id);
            if (userResult != null)
            {
                return true;
            }
            return false;
        }

        public List<EventTracker> GetAllEventTrackers()
        {
            return EventTrackers;
        }

        public EventTracker GetEventTrackerById(int id)
        {
            return EventTrackers.FirstOrDefault(et => et.Id == id);
        }

    }
}
